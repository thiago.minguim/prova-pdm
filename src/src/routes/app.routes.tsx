import React from 'react'

import { Home } from '../screens/Home'
import { Register } from '../screens/Register'
import { Registered } from '../screens/Registered'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

const { Screen, Navigator } = createNativeStackNavigator()

export function AppRoutes() {
  return (
    <Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
      <Screen name="Home" component={Home} />
      <Screen name="Register" component={Register} />
      <Screen name="Registered" component={Registered} />
    </Navigator>
  )
}
