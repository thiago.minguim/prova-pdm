import React, { useEffect } from 'react'
import {
  useNavigation,
  NavigationProp,
  ParamListBase
} from '@react-navigation/native'

import { db } from '../../db'

import { FlatList } from 'react-native'

import { CarCard, CarCardProps } from './components/CarCard'
import { Header, BackButton, Icon, Title } from './styles'

export interface DataListProps extends CarCardProps {
  id: string
}

export function Registered() {
  const { goBack }: NavigationProp<ParamListBase> = useNavigation()
  const [data, setData] = React.useState([] as DataListProps[])

  function loadCar() {
    db.transaction(tx => {
      tx.executeSql('select * from cars', [], (_, { rows }) => {
        setData(rows._array)
      })
    })
  }

  useEffect(() => {
    loadCar()
  }, [])

  return (
    <>
      <Header>
        <BackButton onPress={() => goBack()}>
          <Icon name="chevron-left" />
        </BackButton>
        <Title>Visualização de veiculos</Title>
      </Header>
      <FlatList
        data={data}
        keyExtractor={item => item.id}
        renderItem={({ item }) => <CarCard data={item} />}
      />
    </>
  )
}
