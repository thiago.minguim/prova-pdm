import styled from 'styled-components/native'
import { Entypo } from '@expo/vector-icons'

export const Container = styled.View``

export const Header = styled.View`
  flex-direction: row;
  margin: 60px 40px;
  align-items: center;
`

export const BackButton = styled.TouchableOpacity`
  height: 40px;
  width: 40px;

  border-radius: 10px;
  justify-content: center;
  align-items: center;

  background-color: ${props => props.theme.colors.attention};
`

export const Icon = styled(Entypo)`
  font-size: 24px;
`

export const Title = styled.Text`
  font-size: 22px;
  font-family: ${props => props.theme.fonts.bold};
  margin-left: 20px;

  border-bottom-width: 2px;
`
