import React, { useState } from 'react'
import {
  Container,
  ViewOne,
  Title,
  MinValue,
  ViewTwo,
  Date,
  Icon,
  DescriptionExpand,
  Description
} from './styles'

export interface CarCardProps {
  category: string
  minValue: string
  date: string
  description: string
}

interface Props {
  data: CarCardProps
}

export function CarCard({ data }: Props) {
  const [open, setOpen] = useState(false)

  const toggleOpen = () => {
    setOpen(oldState => !oldState)
  }

  return (
    <Container>
      <ViewOne>
        <Title>{data.category}</Title>
        <MinValue>{data.minValue}</MinValue>
      </ViewOne>
      <ViewTwo>
        <Date>Entrada: {data.date}</Date>
        <DescriptionExpand onPress={toggleOpen}>
          <Icon name="chevron-down" />
        </DescriptionExpand>
      </ViewTwo>
      {open && (
        <Description>Descrição do veiculo: {data.description}</Description>
      )}
    </Container>
  )
}
