import styled from 'styled-components/native'
import { Entypo } from '@expo/vector-icons'

export const Container = styled.View`
  margin: 10px 25px;
  padding: 10px;

  border-radius: 10px;

  background-color: ${props => props.theme.colorCar.blue};
`

export const ViewOne = styled.View`
  flex-direction: row;
  justify-content: space-between;

  padding: 5px;
`

export const Title = styled.Text`
  font-size: 18px;
  color: ${props => props.theme.colors.shape};

  border-bottom-width: 1px;
  border-bottom-color: ${props => props.theme.colors.attention};
`

export const MinValue = styled.Text`
  font-size: 18px;
  color: ${props => props.theme.colors.shape};
`

export const ViewTwo = styled.View`
  flex-direction: row;
  justify-content: space-between;

  padding: 5px;
`

export const Date = styled.Text`
  font-size: 18px;
  color: ${props => props.theme.colors.shape};
`

export const DescriptionExpand = styled.TouchableOpacity``

export const Icon = styled(Entypo)`
  font-size: 18px;
  color: ${props => props.theme.colors.shape};
`

export const Description = styled.Text`
  padding: 5px;

  padding-top: 30px;
  font-size: 14px;
  color: ${props => props.theme.colors.shape};
`
