import React, { useState, useEffect } from 'react'
import { Controller, useForm } from 'react-hook-form'
import * as Yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

import { MaskedInput } from './components/MaskedInput/input'
import DropDownPicker from 'react-native-dropdown-picker'

import {
  Container,
  BackButton,
  Icon,
  Header,
  Title,
  DateContainer,
  Today,
  DateText,
  Button,
  ButtonText,
  Error
} from './styles'

import {
  useNavigation,
  ParamListBase,
  NavigationProp
} from '@react-navigation/native'
import { Keyboard, TouchableWithoutFeedback, View } from 'react-native'
import { Input } from './components/Input/input'
import { db } from '../../db'

const schema = Yup.object().shape({
  category: Yup.string().required('Nome obrigatório'),
  minValue: Yup.string().required('Valor obrigatório'),
  date: Yup.string().required('Data obrigatório'),
  description: Yup.string().required('Descrição obrigatório')
})

export interface FormData {
  category: string
  minValue: string
  date: string
  description: string
}

export function Register() {
  const { navigate }: NavigationProp<ParamListBase> = useNavigation()

  const [open, setOpen] = useState(false)
  const [items, setItems] = useState([
    { label: 'Sedan', value: 'sedan' },
    { label: 'SUV', value: 'suv' },
    { label: 'Camionete', value: 'Camionete' },
    { label: 'Hatch', value: 'hatch' },
    { label: 'Motocicleta', value: 'motocicleta' }
  ])
  const { goBack }: NavigationProp<ParamListBase> = useNavigation()

  useEffect(() => {
    db.transaction(tx => {
      tx.executeSql(
        'create table if not exists cars (id integer primary key not null, category text, minValue text, date text, description text);'
      )
    })
  }, [])

  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors }
  } = useForm<FormData>({
    resolver: yupResolver(schema)
  })

  const setToday = () => {
    const today = new Date()
    const dd = String(today.getDate()).padStart(2, '0')
    const mm = String(today.getMonth() + 1).padStart(2, '0')
    const yyyy = today.getFullYear()
    const date = `${dd}/${mm}/${yyyy}`

    setValue('date', date)
  }

  function register(form: FormData) {
    db.transaction(
      tx => {
        tx.executeSql(
          'insert into cars (category, minValue, date, description) values (?, ?, ?, ?);',
          [form.category, form.minValue, form.date, form.description]
        )
      },
      undefined,
      () => {
        db.transaction(tx => {
          tx.executeSql('select * from cars;', [], (_, { rows }) => {})
        })
      }
    )
    reset()
    navigate('Registered')
  }

  return (
    <>
      <Header>
        <BackButton onPress={() => goBack()}>
          <Icon name="chevron-left" />
        </BackButton>
        <Title>Cadastro de veiculos</Title>
      </Header>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Container>
          <Controller
            control={control}
            name="category"
            render={({ field: { onChange, value } }) => (
              <View>
                <DropDownPicker
                  placeholder="Selecione um veiculo"
                  open={open}
                  value={value}
                  items={items}
                  setOpen={setOpen}
                  setValue={v => {
                    onChange(v())
                  }}
                  setItems={setItems}
                  containerStyle={{
                    borderRadius: 5,
                    width: '100%'
                  }}
                />
                {errors.category && <Error>{errors.category.message}</Error>}
              </View>
            )}
          />

          <MaskedInput
            type={'money'}
            options={{
              precision: 2,
              separator: ',',
              delimiter: '.',
              unit: 'R$',
              suffixUnit: ''
            }}
            control={control}
            name="minValue"
            placeholder="Valor Minimo"
            keyboardType="numeric"
            autoCapitalize="sentences"
            autoCorrect={false}
            error={errors.minValue && errors.minValue.message}
          />
          <DateContainer>
            <MaskedInput
              type={'datetime'}
              options={{
                format: 'DD/MM/YYYY'
              }}
              control={control}
              name="date"
              placeholder="Informe a data"
              error={errors.date && errors.date.message}
              containerStyle={{ width: '80%' }}
            />
            <Today onPress={setToday}>
              <DateText>Hoje</DateText>
            </Today>
          </DateContainer>
          <Input
            control={control}
            name="description"
            placeholder="Informe uma descrição do seu veiculo"
            error={errors.description && errors.description.message}
          />
          <Button onPress={handleSubmit(register)}>
            <ButtonText>Adicionar</ButtonText>
          </Button>
        </Container>
      </TouchableWithoutFeedback>
    </>
  )
}
