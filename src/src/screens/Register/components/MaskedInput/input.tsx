import React from 'react'
import { StyleProp, TextInputProps, ViewStyle } from 'react-native'
import { Control, Controller } from 'react-hook-form'
import { Container, Error, TextInputMask } from './styles'
import {
  TextInputMaskOptionProp,
  TextInputMaskTypeProp
} from 'react-native-masked-text'

import { FormData } from '../..'

interface Props extends TextInputProps {
  control: Control<FormData>
  name: keyof FormData
  error?: string
  type: TextInputMaskTypeProp
  options?: TextInputMaskOptionProp
  containerStyle?: StyleProp<ViewStyle>
}
export function MaskedInput({
  control,
  name,
  error,
  type,
  options,
  containerStyle,
  ...rest
}: Props) {
  return (
    <Container style={containerStyle}>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <TextInputMask
            onChangeText={onChange}
            value={value}
            {...rest}
            type={type}
            options={options}
          />
        )}
        name={name}
      />
      {error && <Error>{error}</Error>}
    </Container>
  )
}
