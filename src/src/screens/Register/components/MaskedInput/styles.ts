import styled from 'styled-components/native'

import { TextInputMask as TextInputMasked } from 'react-native-masked-text'

export const Container = styled.View`
  width: 100%;

  margin: 12px 0;
`

export const TextInputMask = styled(TextInputMasked)`
  padding: 16px 16px;
  font-family: ${props => props.theme.fonts.regular};
  font-size: 14px;
  color: ${props => props.theme.colors.text_dark};

  background-color: ${props => props.theme.colors.shape};
  border-radius: 15px;
  margin-bottom: 8px;

  border-width: 2px;
  border-color: ${props => props.theme.colorCar.blue_light};
`
export const Error = styled.Text`
  font-size: 12px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${props => props.theme.colors.attention};
  margin: 7px 0;
`
