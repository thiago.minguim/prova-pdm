import React from 'react'
import { TextInputProps } from 'react-native'
import { Control, Controller } from 'react-hook-form'
import { Container, Error, TextInput } from './styles'

import { FormData } from '../..'

interface Props extends TextInputProps {
  control: Control<FormData>
  name: keyof FormData
  error?: string
}
export function Input({
  control,
  name,
  error,

  ...rest
}: Props) {
  return (
    <Container>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <TextInput onChangeText={onChange} value={value} {...rest} />
        )}
        name={name}
      />
      {error && <Error>{error}</Error>}
    </Container>
  )
}
