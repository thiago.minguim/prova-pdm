import styled from 'styled-components/native'
import { Entypo } from '@expo/vector-icons'

export const Container = styled.View`
  flex: 1;
  margin: 20px;
`
export const Header = styled.View`
  flex-direction: row;
  margin: 60px 40px;

  align-items: center;
`

export const BackButton = styled.TouchableOpacity`
  height: 40px;
  width: 40px;

  border-radius: 10px;
  justify-content: center;
  align-items: center;

  background-color: ${props => props.theme.colors.attention};
`

export const Icon = styled(Entypo)`
  font-size: 24px;
`

export const Title = styled.Text`
  font-size: 24px;
  font-family: ${props => props.theme.fonts.bold};
  margin-left: 20px;

  border-bottom-width: 2px;
`

export const DateContainer = styled.View`
  width: 100%;

  flex-direction: row;

  justify-content: space-between;
  align-items: center;
`
export const Today = styled.TouchableOpacity`
  background-color: ${props => props.theme.colorCar.blue};
  height: 50px;

  justify-content: center;
  align-items: center;

  width: 50px;
  border-radius: 10px;
`

export const DateText = styled.Text`
  font-family: ${props => props.theme.fonts.bold};
  color: ${props => props.theme.colors.shape};
  font-size: 16px;
`
export const Button = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.attention};
  margin: 100px 20px;
  padding: 20px;
  border-radius: 8px;

  justify-content: center;
  align-items: center;
`
export const ButtonText = styled.Text`
  color: ${props => props.theme.colors.shape};

  font-size: 18px;
  font-family: ${props => props.theme.fonts.bold};
`
export const Error = styled.Text`
  font-size: 12px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${props => props.theme.colors.attention};
  margin: 7px 0;
`
