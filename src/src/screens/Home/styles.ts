import styled from 'styled-components/native'

export const Container = styled.View`
  flex: 1
  background-color: ${props => props.theme.colors.shape};
`
export const Header = styled.View`
  justify-content: center;
  align-items: center;

  margin: 60px 40px;
`

export const Title = styled.Text`
  font-family: ${props => props.theme.fonts.bold};
  font-size: 22px;

  color: ${props => props.theme.colorCar.red};
`

export const SubTitle = styled.Text`
  font-family: ${props => props.theme.fonts.medium};
  font-size: 18px;

  border-bottom-width: 2px;

  color: ${props => props.theme.colorCar.blue};
`
export const ImgCar = styled.Image`
  width: 300px;
  height: 200px;

  margin: 180px 0px;
`
export const ButtonArea = styled.View`
  flex-direction: row;
  margin: 30px 30px;

  justify-content: space-between;
`
export const Touch = styled.TouchableOpacity``
export const Test = styled.Text``
