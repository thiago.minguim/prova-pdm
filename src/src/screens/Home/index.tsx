import React from 'react'
import { Button } from '../../components/Button'
import {
  useNavigation,
  ParamListBase,
  NavigationProp
} from '@react-navigation/native'

import {
  Container,
  Title,
  SubTitle,
  Header,
  ImgCar,
  ButtonArea
} from './styles'

interface Props {
  title: string
  onPress: () => void
}

export function Home({}) {
  const { navigate }: NavigationProp<ParamListBase> = useNavigation()

  return (
    <Container>
      <Header>
        <Title>Concessionária de Seminovos</Title>
        <SubTitle>Pedro Dionísio Matias</SubTitle>
        <ImgCar source={require('../../assets/images/car.png')} />
      </Header>
      <ButtonArea>
        <Button
          title="Visualizar Veiculos"
          onPress={() => navigate('Registered')}
        />
        <Button
          title="Cadastrar Veiculos"
          onPress={() => navigate('Register')}
        />
      </ButtonArea>
    </Container>
  )
}
