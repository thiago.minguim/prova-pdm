export default {
  colors: {
    text: '#969CB2',
    text_dark: '#000',
    background: '#F0F2F5',

    shape: '#FFFFFF',

    sucess: '#12A454',
    sucess_light: 'rgba(18,164,84,0.5)',

    attention: '#E83F5B',
    attention_light: 'rgba(232,63,91,0.5)'
  },

  colorCar: {
    red: '#ED1B1C',
    blue: '#45A6DD',
    blue_light: '#8DD1F4',

    yellow: '#FFCF00'
  },

  fonts: {
    regular: 'Poppins_400Regular',
    medium: 'Poppins_500Medium',
    bold: 'Poppins_700Bold'
  }
}
