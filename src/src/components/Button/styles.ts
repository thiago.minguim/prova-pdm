import styled from 'styled-components/native'

export const Container = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.attention};

  padding: 20px;
  border-radius: 8px;
`
export const Title = styled.Text`
  font-size: 16px;
  color: ${props => props.theme.colors.shape};
`
